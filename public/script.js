var rando;
var yellow = "#f1b300";
var red = "#f93822";
var blue = "#00239c";
var green = "#008330";
var colors = [yellow, red, blue, green];

var styleEl = document.createElement("style");
styleEl.setAttribute("id", "linkColor");
document.querySelector("head").append(styleEl);

var links = document.querySelectorAll("a[href]");
links.forEach(link => {
	link.addEventListener("focus", function () {
		changeLinkColor();
	});

	link.addEventListener("mouseover", function () {
		changeLinkColor();
	});

	function changeLinkColor() {
		rando = Math.floor(Math.random() * colors.length);
		document.getElementById("linkColor").innerHTML = "a[href]:hover,a[href]:focus{ color: " + colors[rando] + "; }a[href] ~ a[href]:hover,a[href] ~ a[href]:focus{ background: " + colors[rando] + " }";
	}
});

